<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_app extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mod_app_model');
	}

	public function get($params = false, $row = false)
	{
		$exec = $this->Mod_app_model->db_get($params, $row);
		if ($exec) {
			return self::result("Data has been found", true, $exec);
		}
		return self::result("Not found!", false, []);
	}

	public function insert($params = false)
	{
		$exec = $this->Mod_app_model->db_insert($params);
		if ($exec) {
			return self::result("Inserted", true, $exec);
		}
		return self::result("Insert failed", false);
	}

	public function delete($params = false)
	{
		$exec = $this->Mod_app_model->db_delete($params);
		if ($exec) {
			return self::result("Deleted", true);
		}
		return self::result("Delete failed", false);
	}

	public function update($params = false)
	{
		$exec = $this->Mod_app_model->db_update($params);
		if ($exec) {
			return self::result("Updated", true);
		}
		return self::result("Update failed", false);
	}
	
	private static function result($message = "Error, ", $success = false, $data = false)
	{
		$result["success"] = $success;
		$result["message"] = $message;
		if ($data) {
			$result["data"] = $data;
		}
		return $result;
	}

}

/* End of file Mod_app.php */
/* Location: ./application/controllers/Mod_app.php */