<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aku extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo "<h1>controller Aku</h1>";
	}

}

/* End of file Aku.php */
/* Location: ./application/controllers/Aku.php */